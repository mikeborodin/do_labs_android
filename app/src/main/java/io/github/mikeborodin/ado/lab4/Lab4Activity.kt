package io.github.mikeborodin.ado.lab4

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TableLayout
import android.widget.TableRow
import io.github.mikeborodin.ado.R
import io.github.mikeborodin.ado.lab3.solveTransportDiff
import kotlinx.android.synthetic.main.activity_lab4.solveDiffBtn
import kotlinx.android.synthetic.main.activity_main.cols
import kotlinx.android.synthetic.main.activity_main.okBtn
import kotlinx.android.synthetic.main.activity_main.output
import kotlinx.android.synthetic.main.activity_main.rows
import kotlinx.android.synthetic.main.activity_main.table
import java.io.ByteArrayOutputStream
import java.io.PrintStream

class Lab4Activity : AppCompatActivity() {

    var matrix = arrayOf(
            floatArrayOf(4f, 2f, 4.1f, 6f, 17f),
            floatArrayOf(5f, 2.5f, 2f, 3f, 73f),
            floatArrayOf(3f, 4f, 3f, 4.2f, 52f),
            floatArrayOf(5.1f, 3f, 2f, 7f, 38f),
            floatArrayOf(37f, 35f, 86f, 22f, 0f)
    )

    var matrix5 = arrayOf(
            floatArrayOf(10f, 7f, 4f, 1f, 4f, 100f),
            floatArrayOf(2f, 7f, 10f, 6f, 11f, 250f),
            floatArrayOf(8f, 5f, 3f, 3f, 2f, 200f),
            floatArrayOf(11f, 8f, 12f, 16f, 13f, 300f),
            floatArrayOf(200f, 200f, 100f, 100f, 250f, 0f)
    )
    var matrix54 = arrayOf(
            floatArrayOf(5f, 1.8f, 6f, 6f, 32f),
            floatArrayOf(1f, 5.1f, 8f, 2f, 42f),
            floatArrayOf(3.5f, 6f, 3f, 3.1f, 10f),
            floatArrayOf(2.2f, 4.9f, 1.3f, 4f, 16f),
            floatArrayOf(3f, 7f, 8.95f, 1f, 10f),
            floatArrayOf(20f, 38f, 30f, 22f, 0f)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Транспортна задача \uD83D\uDE99"
        setContentView(R.layout.activity_lab4)

        okBtn.setOnClickListener {
            val r = rows.text.toString().toInt()
            val c = cols.text.toString().toInt()

            fillTable(r + 1, c + 1, buildMatrix(r, c)!!, table)
        }

        /*solveNWBtn.setOnClickListener {
            //gomory(matrix)
            matrix54?.let {
                val baos = ByteArrayOutputStream()
                val ps = PrintStream(baos)
                val old = System.out
                System.setOut(ps)

//                solveTransportNW(matrix5)
                solveTransportNW(matrix54)

                System.out.flush()
                System.setOut(old)
                println(baos.toString())
                output.text = baos.toString()
            }
        }*/

        /*solveMinBtn.setOnClickListener {
            //gomory(matrix)
            matrix54?.let {
                val baos = ByteArrayOutputStream()
                val ps = PrintStream(baos)
                val old = System.out
                System.setOut(ps)

//                solveTransportMin(matrix5)
                solveTransportMin(matrix54)

                System.out.flush()
                System.setOut(old)
                println(baos.toString())
                output.text = baos.toString()
            }
        }*/

        solveDiffBtn.setOnClickListener {
            //gomory(matrix)
            matrix?.let {
                val baos = ByteArrayOutputStream()
                val ps = PrintStream(baos)
                val old = System.out
                System.setOut(ps)

                solveTransportDiff(matrix)
//                solveTransportDiff(matrix54)

                System.out.flush()
                System.setOut(old)
                println(baos.toString())
                output.text = baos.toString()
            }
        }

        okBtn.callOnClick()
        solveDiffBtn.callOnClick()
    }


    private fun buildMatrix(r: Int, c: Int): Array<FloatArray>? {
        var matrix: Array<FloatArray>

        if (r == 4 && c == 5) {
            return this.matrix5
        } else if (r == 4 && c == 4) {
            return this.matrix
        } else if (r == 5 && c == 4) {
            return this.matrix54
        } else {
            matrix = Array(r + 1, { j -> FloatArray(c + 1) })
        }
        return matrix
    }

    private fun fillTable(r: Int, c: Int, matrix: Array<FloatArray>, table: TableLayout) {
        table.removeAllViews()
        for (i in 0 until r) {
            val row = TableRow(this@Lab4Activity)
            row.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)

            for (j in 0 until c) {
                val edit = EditText(this@Lab4Activity).apply {
                    width = 300
                    inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
                    layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
                }

                edit.setText(matrix[i][j].toString())

                edit.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        s?.toString()?.takeIf { it.isNotEmpty() }?.let {
                            try {
                                matrix[i][j] = it.toFloat()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                })
                row.addView(edit)
            }
            table.addView(row)
        }
    }
}
