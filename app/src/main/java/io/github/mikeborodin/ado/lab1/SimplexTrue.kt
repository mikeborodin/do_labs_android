/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.lang.Float.isNaN
import java.lang.Math.pow
import java.lang.System.out


object SimplexTrue {
    // загальна кількість змінних
    var allVars = 4
    // количество базисных переменных
    var basisVars = 2

    fun solve(coefsF: FloatArray,
              coefsLimit: Array<FloatArray>,
              basisCoefs: FloatArray,
              basisVars: IntArray,
              basisSol: FloatArray): FloatArray {

        return simplexMaxMethod(coefsF, coefsLimit, basisCoefs, basisVars, basisSol)
    }

    /**
     * Симплекс-метод. Підрахунок максимуму.
     */

    var coefsF: FloatArray? = null

    fun simplexMaxMethod(cFunc: FloatArray, cL: Array<FloatArray>,
                         cB: FloatArray, bVars: IntArray,
                         bS: FloatArray): FloatArray {

        coefsF = cFunc.clone()

        val delta = FloatArray(allVars)
        val min = FloatArray(basisVars)

        System.out.print("\n\nЗадача максимізації функції: F=")
        coefsF!!.forEachIndexed { ind, v ->
            print("+${v}*x${ind}")
        }
        println("\n\n")

        var i: Int
        var j: Int

        var r: Int
        var s = -1

        val tmpA = Array(basisVars, { FloatArray(allVars) })
        val tmpBS = FloatArray(basisVars)

        var k = 0
        while (true) {
            ++k
            // Рахуєм дельти, виділяємо головний стовпець
            // (стовпець с максимальною оцінкою)
            var deltaMax = java.lang.Float.NEGATIVE_INFINITY
            r = -1
            j = 0
            while (j < allVars) {
                var z = 0.0
                i = 0
                while (i < basisVars) {
                    z += cB[i] * cL[i][j]
                    ++i
                }
                delta[j] = (cFunc[j] - z).toFloat()
                if (deltaMax < delta[j]) {
                    deltaMax = delta[j]
                    r = j
                }
                ++j
            }

            //Умова виходу (всі оцінки непозитивні)
            if (deltaMax <= 0 || k > 100) {
                break
            }

            // Визначаємо головний рядок (з мінімальним відношенням
            // bS[i] / cL[i][r])
            var minRow = java.lang.Double.POSITIVE_INFINITY
            s = -1
            i = 0
            while (i < basisVars) {
                min[i] = bS[i] / cL[i][r]
                if (min[i] < 0) {
                    min[i] = java.lang.Float.NaN
                }
                if (minRow > min[i]) {
                    minRow = min[i].toDouble()
                    s = i
                }
                ++i
            }

            val element = cL[s][r]

            out.println("Головний стовпець  r = " + (r + 1))
            out.println("Головний рядок     s = " + (s + 1))
            out.println("Головний елемент Xrs = " + round(element, 2) + "\n")

            printTable(cFunc, cL, cB, bVars, bS, min)

            //Головний елемент (на перетині головного рядка і стовпця)

            // Зберігаємо вміст масивів (тому що їх значення в ході
            // обчислення змінюються, але залишаються потрібними для цих обчислень)
            i = 0
            while (i < basisVars) {
                tmpA[i] = FloatArray(allVars)
                j = 0
                while (j < allVars) {
                    tmpA[i][j] = cL[i][j]
                    ++j
                }
                tmpBS[i] = bS[i]
                ++i
            }

            // Заносимо змінну в базис,
            // Перераховуємо базисне рішення і коефіцієнтів змінних -
            // - в головному рядку


            bVars[s] = r
            cB[s] = cFunc[r]
            j = 0
            while (j < allVars) {
                cL[s][j] /= element
                ++j
            }
            bS[s] /= element

            // - в інших рядках
            i = 0
            while (i < basisVars) {
                // головний рядок вже перераховано
                if (i == s) {
                    ++i
                    continue
                }

                // элемент головного стовпця
                val a_ir = tmpA[i]!![r]

                // перераховуєм коефіцієнти
                j = 0
                while (j < allVars) {
                    cL[i][j] -= a_ir * tmpA[s][j] / element
                    ++j
                }

                // перерахунок базисного рішення
                bS[i] -= a_ir * tmpBS[s] / element
                ++i
            }

            printDelta(delta)
            out.println("-----------------------------\n")
        }

        out.println("Головний стовпець  r = " + (r + 1))
        out.println("Головний рядок     s = " + (s + 1) + "\n")
        printTable(cFunc, cL, cB, bVars, bS, min)
        printDelta(delta)
        return printSolution(bVars, bS)
    }

    /**
     * Виводимо рішення
     */
    internal fun printSolution(bV: IntArray, basisSol: FloatArray): FloatArray {
        var i: Int
        var j: Int = 0
        var zm: Float = 0f
        out.println("Всі оцінки невід'ємні, обчислення завершено")
        out.print("Розв'язок X = (")
        val res = FloatArray(basisVars)
        var f: Boolean
        while (j < allVars) {
            f = false
            i = 0
            while (i < basisVars) {
                if (bV[i] == j) {
                    if (j < basisVars) {
                        res[j] = basisSol[i]
                    }
                    //                    if(i==0){
                    //                     zm=zm+16*basisSol[i];
                    //                    }
                    //                    if(i==1){
                    //                        zm=zm+12*basisSol[i];
                    //                    }
                    out.print(round(basisSol[i], 2))
                    f = true
                    break
                }
                ++i
            }
            if (!f) {
                out.print("0")
            }
            if (j < allVars - 1) {
                out.print(", ")
            }
            ++j
        }
        out.println(")")
        basisSol.forEachIndexed { ind, v ->
            zm += v * coefsF!![ind]
        }
        out.println("F = " + round(zm, 2))

        return res
    }

    /**
     * Виводимо рядок оцінок
     */
    internal fun printDelta(delta: FloatArray) {
        out.print("\t\t\t\t")
        for (j in 0 until allVars) {
            out.print(round(delta[j], 2) + "\t")
        }
        //  out.println("delta[j]");
        out.println()
    }

    /**
     * Виводимо таблицю
     */
    internal fun printTable(c: FloatArray, a: Array<FloatArray>, cB: FloatArray, bV: IntArray,
                            bS: FloatArray, min: FloatArray) {
        var i: Int = 0
        var j: Int
        // вивід: рядок коефіцієнтів
        out.print("\t\t\t\t")
        j = 0
        while (j < allVars) {
            out.print(round(c[j], 2) + "\t")
            ++j
        }
        out.println("c[j]")

        // вивід: ряд x
        out.print("\t\t\t\tbV\t\t\tbS\t\t")
        j = 0
        while (j < allVars) {
            out.print("x[" + (j + 1) + "] ")
            ++j
        }
        out.println("bSi/a[s][r]")

        while (i < basisVars) {
            out.print("\t" /*+ round(cB[i], 2)*/ + "\tx[" + (bV[i] + 1) + "]\t\t\t"
                    + round(bS[i], 2) + "\t\t\t")
            j = 0
            while (j < allVars) {
                out.print(round(a[i][j], 2) + "\t\t\t")
                ++j
            }
            out.println(round(min[i], 2))
            ++i
        }

        var zm = 0f
        bS.forEachIndexed { ind, v ->
            zm += v * coefsF!![ind]
        }
        out.println("F = " + round(zm, 2))


    }

    internal fun printArray(ar: FloatArray) {
        out.println()
        for (i in ar.indices) {
            out.println("\t[" + i + "] = " + ar[i])
        }
        out.println()
    }

    internal fun round(n: Float, p: Int): String {
        if (isNaN(n)) {
            return "NaN"
        }
        if (java.lang.Float.isInfinite(n)) {
            return "\u221E"
        }
        val d = pow(10.0, p.toDouble())
        return (Math.round(n * d) / d).toString() + ""
    }


    /*
    *
    /**
     * Симплекс-метод. Підрахунок мінімуму.
     */
    internal fun simplexMinMethod(c: FloatArray, a: Array<FloatArray>, cB: FloatArray, bV: IntArray,
                                  bS: FloatArray): FloatArray {
        val delta = FloatArray(allVars)
        val min = FloatArray(basisVars)

        out.println("\n\nПошук мінімума\n\n")
        var i: Int
        var j: Int
        var r = -1
        var s = -1
        val tmpA = arrayOfNulls<FloatArray>(basisVars)
        val tmpBD = FloatArray(basisVars)

        var k = 0
        while (true) {
            ++k

            // Рахуєм дельти, виділяєм головний стовпець
            // (стовпець з мінімальною оцінкою)
            var deltaMin = java.lang.Double.POSITIVE_INFINITY
            r = -1
            j = 0
            while (j < allVars) {
                var z = 0.0
                i = 0
                while (i < basisVars) {
                    z += cB[i] * a[i][j]
                    ++i
                }
                delta[j] = c[j] - z
                if (deltaMin > delta[j]) {
                    deltaMin = delta[j]
                    r = j
                }
                ++j
            }

            // Умова виходу (всі оцінки невід'ємні)
            if (deltaMin >= 0 || k > 100) {
                break
            }

            // Визначаємо головний рядок (з мінімальним відношенням)
            // bS[i] / a[i][r])
            var minRow = java.lang.Double.POSITIVE_INFINITY
            s = -1
            i = 0
            while (i < basisVars) {
                min[i] = bS[i] / a[i][r]
                if (min[i] < 0) {
                    min[i] = java.lang.Double.NaN
                }
                if (minRow > min[i]) {
                    minRow = min[i]
                    s = i
                }
                ++i
            }

            out.println("Головний стовпець: r = " + r)
            out.println("Головний рядок:  s = " + s)
            printTable(c, a, cB, bV, bS, min)

            // Головний елемент (на перетині головного рядка і стовпця)
            val element = a[s][r]

            // Зберігаємо вміст масивів (тому що їх значення в ході
            //обчислення змінюються, але залишаються потрібними для цих обчислень)
            i = 0
            while (i < basisVars) {
                tmpA[i] = FloatArray(allVars)
                j = 0
                while (j < allVars) {
                    tmpA[i]!![j] = a[i][j]
                    ++j
                }
                tmpBD[i] = bS[i]
                ++i
            }

            // Вносимо змінну в базис,
            // Перераховуємо базисне рішення і коефіцієнтів змінних -

            // - в головному рядку
            bV[s] = r
            cB[s] = c[r]
            j = 0
            while (j < allVars) {
                a[s][j] /= element
                ++j
            }
            bS[s] /= element

            // - в інших рядках
            i = 0
            while (i < basisVars) {
                // головний рядок уже перерахований
                if (i == s) {
                    ++i
                    continue
                }

                // элемент головного стовпця
                val air = tmpA[i]!![r]

                // перерахунок коефіцієнтів
                j = 0
                while (j < allVars) {
                    a[i][j] -= air * tmpA[s]!![j] / element
                    ++j
                }

                // перерахунок базисного рішення
                bS[i] -= air * tmpBD[s] / element
                ++i
            }

            printDelta(delta)
            out.println("----------------------------------------------------")
        }

        out.println("Головний стовпець: r = " + (r + 1))
        out.println("Головний рядок:  s = " + (s + 1))
        printTable(c, a, cB, bV, bS, min)
        printDelta(delta)
        return printSolution(bV, bS)
    }

    * */
}
