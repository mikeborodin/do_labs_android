package io.github.mikeborodin.ado.lab1

import io.github.mikeborodin.ado.lab1.Simplex.StopCase.NOT_OPTIMAL
import io.github.mikeborodin.ado.lab1.Simplex.StopCase.OPTIMAL
import io.github.mikeborodin.ado.lab1.Simplex.StopCase.UNBOUNDED
import io.github.mikeborodin.ado.lab2.DSimplex
import io.github.mikeborodin.ado.lab2.DSimplexLauncher
import kotlin.math.abs
import kotlin.math.round

object SimplexLauncher {

    private val onSolved: (mat: Simplex) -> Unit = { simplex ->
        //Step1
        println("onSolved F = ${simplex.table.last().last()}")

        val ratios = simplex.table.filterIndexed { i, _ -> i != simplex.table.lastIndex }
                .map { it.last() }.toFloatArray()

        if (!ratios.isEmpty()) {
            //find max ratio
            var maxLine = 0
            var maxV = 0f
            ratios.map { it - Math.floor(it.toDouble()) }.forEachIndexed { index, fl ->
                if (maxV < fl) {
                    maxV = fl.toFloat()
                    maxLine = index
                }
            }


            val newRow = FloatArray(simplex.table[maxLine].size + 1, { 0f })
            newRow[newRow.lastIndex] = -maxV

            for (i in 0 until simplex.table[maxLine].size - 1) {
                newRow[i] = -1 * simplex.table[maxLine][i]
            }
            //replace old 1
            if (newRow.indexOf(-1f) != -1)
                newRow[newRow.indexOf(-1f)] = 0f

            newRow[newRow.lastIndex - 1] = 1f

            var mat = buildNextTable(simplex.table, newRow)

            val matForD = convertMat(mat)

            println("Макс. дробова частина на у рівняння №$maxLine")

            matForD?.let {
                DSimplexLauncher.solvePrepared(it.size - 1, it.size + 1, it, { res ->
                    onResult(res)
                })
            }
        }
    }

    fun onResult(simplex: DSimplex) {
        val ratios = simplex.table.filterIndexed { i, _ -> i != simplex.table.lastIndex }
                .map { it.first() }.toFloatArray()

        if (!ratios.isEmpty() && ratios.any { it > 0.1f }) {
            //find max ratio
            var maxLine = 0
            var maxV = 0f
            ratios.map { it - Math.round(it.toDouble()) }.forEachIndexed { index, fl ->
                if (maxV < fl) {
                    maxV = fl.toFloat()
                    maxLine = index
                }
            }
            println("Макс. дробова частина на у рівняння №$maxLine")

            if (abs(maxV - round(maxV)) < 0.1) {
                println("Достатньо точно!")
                return
            }


            //Step2
            val newRow = FloatArray(simplex.table[maxLine].size + 1, { 0f })
            newRow[0] = -maxV

            for (i in 1 until simplex.table[maxLine].size) {
                newRow[i] = -1 * simplex.table[maxLine][i]
            }
            //replace old 1
            if (newRow.indexOf(-1f) != -1 && newRow.indexOf(-1f) != 0)
                newRow[newRow.indexOf(-1f)] = 0f
            if (newRow.indexOf(1f) != -1 && newRow.indexOf(-1f) != 0)
                newRow[newRow.indexOf(1f)] = 0f

            newRow[newRow.lastIndex] = 1f

            var mat = buildNextTableD(simplex.table, newRow)

//            val matForD = convertMat(mat)

            println("Макс. дробова частина на у рівняння №$maxLine")

            mat?.let {
                DSimplexLauncher.solvePrepared(it.size - 1, it.size + 1, it, { res ->
                    onResult(res)
                })
            }
        }
    }


    fun convertMat(mat: Array<FloatArray>): Array<FloatArray> {
        var res = arrayOf<FloatArray>()

        mat.forEach {
            val copyRow = FloatArray(it.size, { i -> it[i] })
            shiftArray(copyRow, 1)
            res += copyRow
        }
        return res
    }


    private fun buildNextTable(table: Array<FloatArray>, newRow: FloatArray): Array<FloatArray> {
        var mat = Array(table.size - 1, { FloatArray(newRow.size, { 0f }) })

        for (i in 0 until table.size - 1) {
            for (j in 0 until table[i].size - 1) {
                mat[i][j] = table[i][j]
            }
            mat[i][mat[i].lastIndex] = table[i].last()
        }

        mat += newRow
        mat += table.last()

        for (i in 0 until mat.last().size) {
            mat[mat.lastIndex][i] *= -1f
        }
        return mat
    }

    private fun buildNextTableD(table: Array<FloatArray>, newRow: FloatArray): Array<FloatArray> {
        var mat = Array(table.size - 1, { FloatArray(newRow.size, { 0f }) })

        for (i in 0 until table.size - 1) {
            mat[i][0] = table[i].first()
            for (j in 1 until table[i].size) {
                mat[i][j] = table[i][j]
            }
        }

        mat += newRow
        mat += table.last()

        for (i in 0 until mat.last().size) {
            mat[mat.lastIndex][i] *= -1f
        }
        return mat
    }

    fun solveGomory(basisCount: Int, allCount: Int, matrix: Array<FloatArray>) {
        solve(basisCount, allCount, matrix, {})
    }

    fun solve(basisCount: Int, allCount: Int, matrix: Array<FloatArray>, func: (FloatArray) -> Unit) {
        var quit = false
        val mat = Array(basisCount + 1, { FloatArray(allCount + 1, { 0f }) })
        (0 until basisCount).forEach { i ->
            (0 until allCount - basisCount).forEach { j ->
                mat[i][j] = matrix[i][j]
            }

            val add = allCount - basisCount
            (0 until basisCount).forEach { j ->
                mat[i][i + add] = 1f
            }
            mat[i][mat[i].lastIndex] = matrix[i].last()
        }
        (0 until matrix[0].size).forEach {
            mat[mat.lastIndex][it] = -matrix[matrix.lastIndex][it]
        }

        val simplex = Simplex(basisCount, allCount).apply {
            buildTable(mat)
        }

        println("Вхідні дані:\t\t\t")
        simplex.print()

        while (!quit) {
            val err = simplex.processStep()

            when (err) {
                NOT_OPTIMAL -> {
                    simplex.print()
                    println("F(X)= ${simplex.table.last().last()}")
                    println("Розв'язок нє оптимальним, продовжуєм")
                }
                OPTIMAL -> {
                    simplex.print()

                    println("Розв'язок оптимальний!\nЗавершуємо обчислення\n")
                    //println("X = ${Arrays.toString(simplex.table.last().copyOfRange(0, allCount))}")
                    println("F(X)= ${String.format("%.1f", simplex.table.last().last())}")
                    //onSolved.invoke(simplex)

                    func.invoke(simplex.table.filterIndexed { index, floats ->
                        index != simplex.table.lastIndex
                    }.sortedBy { it.indexOf(1f) }.map { it.last() }
                            .toFloatArray())
                    quit = true
                }
                UNBOUNDED -> {
                    println("Розв'язок не є обмеженим")
                    quit = true
                }
            }
        }
    }


    //Helpers
    fun gcd(a: Int, b: Int): Int {
        var a = a
        var b = b
        while (b != 0) {
            val c = a
            a = b
            b = c % a
        }
        return a
    }

    fun shiftArray(A: FloatArray, n: Int) {
        var n = n
        val N = A.size
        n %= N
        if (n < 0)
            n += N
        val d = gcd(N, n)
        for (i in 0 until d) {
            val temp = A[i]
            var j = i - n + N
            while (j != i) {
                A[(j + n) % N] = A[j]
                j = (j - n + N) % N
            }
            A[i + n] = temp
        }
    }
}
