package io.github.mikeborodin.ado.lab5

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TableLayout
import android.widget.TableRow
import io.github.mikeborodin.ado.R
import kotlinx.android.synthetic.main.activity_lab5.okBtn
import kotlinx.android.synthetic.main.activity_lab5.output
import kotlinx.android.synthetic.main.activity_lab5.rows
import kotlinx.android.synthetic.main.activity_lab5.solveDijkstraBtn
import kotlinx.android.synthetic.main.activity_lab5.solveFloydBtn
import kotlinx.android.synthetic.main.activity_lab5.table
import java.io.ByteArrayOutputStream
import java.io.PrintStream

class Lab5Activity : AppCompatActivity() {

    val task = Task(
            listOf(
                    Edge("1", "2", 5),
                    Edge("1", "3", 4),
                    Edge("1", "4", 3),

                    Edge("2", "5", 4),

                    Edge("3", "5", 7),

                    Edge("4", "6", 7),

                    Edge("5", "6", 2),

                    Edge("6", "7", 4),
                    Edge("7", "5", 6)

            )
            , "1"
            , "7"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Потоки в мережах ☸️️"
        setContentView(R.layout.activity_lab5)

        okBtn.setOnClickListener {
            val size = rows.text.toString().toInt()
            fillTaskTable(size, buildTask(size), table)
        }

        solveFloydBtn.setOnClickListener {
            task?.let {
                val baos = ByteArrayOutputStream()
                val ps = PrintStream(baos)
                val old = System.out
                System.setOut(ps)

                solveNetworkStreamFloyd(task)

                System.out.flush()
                System.setOut(old)
                println(baos.toString())
                output.text = baos.toString()
            }
        }

        solveDijkstraBtn.setOnClickListener {
            //gomory(matrix)
            task?.let {
                val baos = ByteArrayOutputStream()
                val ps = PrintStream(baos)
                val old = System.out
                System.setOut(ps)

                solveNetworkStreamDijkstra(task)

                System.out.flush()
                System.setOut(old)
                println(baos.toString())
                output.text = baos.toString()
            }
        }

        okBtn.callOnClick()
//        solveDijkstraBtn.callOnClick()
        solveFloydBtn.callOnClick()
    }

    private fun buildTask(size: Int): Task {
        return if (size == 9)
            this.task
        else
            Task(ArrayList(size), "", "")
    }

    private fun fillTaskTable(size: Int, task: Task, table: TableLayout) {
        table.removeAllViews()

        val fromToRow = TableRow(this@Lab5Activity).apply {
            layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
        }
        //add START v
        val editFrom = EditText(this@Lab5Activity).apply {
            width = 400
            hint = "Start"
            inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
            layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
        }
        editFrom.setText(task.start)
        editFrom.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.toString()?.takeIf { it.isNotEmpty() }?.let {
                    try {
                        task.start = it
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        })
        fromToRow.addView(editFrom)


        //add START v
        val editTo = EditText(this@Lab5Activity).apply {
            width = 400
            hint = "Finish"
            inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
            layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
        }
        editTo.setText(task.end)
        editTo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.toString()?.takeIf { it.isNotEmpty() }?.let {
                    try {
                        task.end = it
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        })
        fromToRow.addView(editTo)
        table.addView(fromToRow)



        for (i in 0 until size) {
            val row = TableRow(this@Lab5Activity).apply {
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
            }

            //add V1
            val editV1 = EditText(this@Lab5Activity).apply {
                width = 400
                inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
            }
            editV1.setText(task.edges[i].v1)
            editV1.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s?.toString()?.takeIf { it.isNotEmpty() }?.let {
                        try {
                            task.edges[i].v1 = it
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            })
            row.addView(editV1)


            //add V2
            val editV2 = EditText(this@Lab5Activity).apply {
                width = 400
                inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
            }
            editV2.setText(task.edges[i].v2)
            editV2.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s?.toString()?.takeIf { it.isNotEmpty() }?.let {
                        try {
                            task.edges[i].v2 = it
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            })
            row.addView(editV2)


            //add V2
            val editDist = EditText(this@Lab5Activity).apply {
                width = 400
                inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
                layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
            }
            editDist.setText(task.edges[i].dist.toString())
            editDist.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s?.toString()?.takeIf { it.isNotEmpty() }?.let {
                        try {
                            task.edges[i].dist = it.toInt()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            })
            row.addView(editDist)



            table.addView(row)
        }
    }
}
