package io.github.mikeborodin.ado.lab2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TableLayout
import android.widget.TableRow
import io.github.mikeborodin.ado.R.layout
import kotlinx.android.synthetic.main.activity_lab2.solveBtn
import kotlinx.android.synthetic.main.activity_main.cols
import kotlinx.android.synthetic.main.activity_main.okBtn
import kotlinx.android.synthetic.main.activity_main.output
import kotlinx.android.synthetic.main.activity_main.rows
import kotlinx.android.synthetic.main.activity_main.table
import java.io.ByteArrayOutputStream
import java.io.PrintStream

class Lab2Activity : AppCompatActivity() {

    var matrix: Array<FloatArray>? = null

    var default: Array<FloatArray>? = arrayOf(
            floatArrayOf(50f, 1f, 0f),
            floatArrayOf(80f, 0f, 1f),

            floatArrayOf(90f, 3f, -2f),
            floatArrayOf(10f, 1f, -2f),
            floatArrayOf(50f, -2f, 1f),
            floatArrayOf(-10f, -1f, -1f),
            floatArrayOf(-20f, -1f, -4f),

            floatArrayOf(0f, -7f, -2f) //F -> Max
    )

    var default2: Array<FloatArray>? = arrayOf(
            floatArrayOf(-1f, 2f, -10f),
            floatArrayOf(1f, 1f, 105f),
            floatArrayOf(-2f, 1f, 50f),
            floatArrayOf(1f, 7f, 0f))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Simplex Method Lab2"

        setContentView(layout.activity_lab2)

        okBtn.setOnClickListener {
            val r = rows.text.toString().toInt()
            val c = cols.text.toString().toInt()
            matrix = buildMatrix(r, c)

            fillTable(r, c, matrix!!, table)
        }

        solveBtn.setOnClickListener {
            //gomory(matrix)
            matrix?.let {
                val baos = ByteArrayOutputStream()
                val ps = PrintStream(baos)
                val old = System.out
                System.setOut(ps)
                DSimplexLauncher.solve(it.size - 1, it.size + 1, it)
                System.out.flush()
                System.setOut(old)
                println(baos.toString())
                output.text = baos.toString()
            }
        }

        okBtn.callOnClick()
        solveBtn.callOnClick()
    }


    private fun buildMatrix(r: Int, c: Int): Array<FloatArray>? {
        var matrix: Array<FloatArray> = arrayOf()
        /*if (r == 3 && c == 3) { //my costs task is here
            return default2
        }*/


        if (r == 7 && c == 3) { //my costs task is here
            return default
        } else
            for (i in 0 until r) {
                matrix += FloatArray(c, { 0f })

            }
        return matrix
    }

    private fun fillTable(r: Int, c: Int, matrix: Array<FloatArray>, table: TableLayout) {
        table.removeAllViews()
        for (i in 0 until r) {
            val row = TableRow(this@Lab2Activity)
            row.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)

            for (j in 0 until c) {
                val edit = EditText(this@Lab2Activity).apply {
                    width = 300
                    inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
                    layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
                }

                edit.setText(matrix[i][j].toString())

                edit.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        s?.toString()?.takeIf { it.isNotEmpty() }?.let {
                            try {
                                matrix[i][j] = it.toFloat()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }

                })
                row.addView(edit)
            }
            table.addView(row)
        }
    }

}
