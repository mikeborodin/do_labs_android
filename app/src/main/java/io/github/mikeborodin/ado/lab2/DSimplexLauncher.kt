package io.github.mikeborodin.ado.lab2

object DSimplexLauncher {

    fun solve(basisCount: Int, allCount: Int, matrix: Array<FloatArray>) {
        var quit = false
        val mat = Array(basisCount + 1, { i ->
            FloatArray(allCount + 1,
                    { j ->
                        if (j >= matrix[0].size) 0f else
                            matrix[i][j]
                    })
        })

        (0 until basisCount).forEach { i ->
            mat[i][i + matrix[0].size] = 1f
        }

        val simplex = DSimplex(basisCount, allCount).apply {
            buildTable(mat)
        }

        println("Вхідні дані:\t\t\t")
        simplex.print()

        while (!quit) {
            val err = simplex.processStep()

            when (err) {
                DSimplex.StopCase.NOT_OPTIMAL -> {
                    simplex.print()
                    println("F(X)= ${simplex.table.last().first()}")
                    println("Розв'язок нє оптимальним, продовжуєм")
                }
                DSimplex.StopCase.OPTIMAL -> {
                    simplex.print()

                    println("Розв'язок оптимальний!\n\n")
                    println("F(X)= ${simplex.table.last().first()}")
                    quit = true
                }
                DSimplex.StopCase.UNBOUNDED -> {
                    println("Розв'язок не є обмеженим")
                    quit = true
                }
            }
        }
    }


    //Solve 2

    fun solvePrepared(basisCount: Int, allCount: Int, mat: Array<FloatArray>, onSolved: (DSimplex) -> Unit) {
        var quit = false

        val simplex = DSimplex(basisCount, allCount).apply {
            buildTable(mat)
        }

        println("Вхідні дані:\t\t\t")
        simplex.print()

        while (!quit) {
            val err = simplex.processStep()

            when (err) {
                DSimplex.StopCase.NOT_OPTIMAL -> {
                    simplex.print()
                    println("F(X)= ${simplex.table.last().first()}")
                    println("Розв'язок нє оптимальним, продовжуєм")
                }
                DSimplex.StopCase.OPTIMAL -> {
                    simplex.print()
                    onSolved.invoke(simplex)
                    println("Розв'язок оптимальний!\n\n")
                    //println("F(X)= ${simplex.table.last().first()}")
                    quit = true
                }
                DSimplex.StopCase.UNBOUNDED -> {
                    println("Розв'язок не є обмеженим")
                    quit = true
                }
            }
        }
    }
}
