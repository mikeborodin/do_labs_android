package io.github.mikeborodin.ado.lab1

import io.github.mikeborodin.ado.lab1.Simplex.StopCase.NOT_OPTIMAL
import io.github.mikeborodin.ado.lab1.Simplex.StopCase.OPTIMAL
import io.github.mikeborodin.ado.lab1.Simplex.StopCase.UNBOUNDED


class Simplex(var basisCount: Int,var allCount: Int) {

    private val rows: Int = basisCount + 1
    private val cols: Int = allCount + 1
    private val basis = IntArray(basisCount)

    val table: Array<FloatArray>

    private var solutionIsUnbounded = false

    enum class StopCase {
        NOT_OPTIMAL,
        OPTIMAL,
        UNBOUNDED
    }

    init {
        table = Array(rows, { FloatArray(cols) })
    }

    fun print() {
        for (i in 0 until rows) {
            for (j in 0 until cols) {
                val value = String.format("%.2f", table[i][j])
                print(value + "\t\t\t")
            }
            println()
        }
        println()

        for (i in 0 until basisCount) {
            basis[i] = table[i].indexOf(1f)
            println("Basis $i = X${basis[i]+1}")
        }

    }

    fun buildTable(data: Array<FloatArray>) {
        for (i in table.indices) {
            System.arraycopy(data[i], 0, this.table[i], 0, data[i].size)
        }
    }

    // обчислює значення для наступонї тпблиці
    // використовується у циклі поки не вийде з умови
    fun processStep(): StopCase {
// крок 1
        if (isOptimal()) {
            println("Весь b стовпець невід'ємний => Optimal, закінчуєм")
            return OPTIMAL // solution is optimal
        }

        // крок 2
        // знайдемо вхідний стовпець
        val pivotColumn = findMainCol(table.last())
        val pivotRow = findMainRow(pivotColumn)
        println("Головний рядок: $pivotRow")

        // крок 3
        // починаємо пошук головного елемента

        if (solutionIsUnbounded) {
            return UNBOUNDED
        }

        System.out.println("Гол стовпець: $pivotColumn");
        System.out.println("Головний елемент X[$pivotRow][$pivotColumn]= ${String.format("%.2f",
                table[pivotRow][pivotColumn])}\n\n")

        // крок 4
        // формуємо таблицю
        rectRule(pivotRow, pivotColumn)
        return NOT_OPTIMAL
    }

    private fun findMainRow(column: Int): Int {
        val assets = FloatArray(rows)
        val res = FloatArray(rows)
        var allNegativeCount = 0

        for (i in 0 until rows) {
            if (table[i][column] > 0) {
                assets[i] = table[i][column]
            } else {
                assets[i] = 0f
                allNegativeCount++
            }
        }

        if (allNegativeCount == rows) {
            this.solutionIsUnbounded = true
        } else {
            println("Bідношення:\n")
            for (i in 0 until rows) {
                val v = assets[i]
                if (v > 0) {
                    res[i] = table[i][cols - 1] / v
                    println("${String.format("%.2f", v)}>0 => ${String.format("%.2f", table[i][cols - 1])} / ${String.format("%.2f", v)} = ${String.format("%.2f", res[i])}\t")
                }
            }

        }

        var idx = 0
        var minv = res[0]
        var min = Float.POSITIVE_INFINITY

        for (i in 0 until rows) {
            minv = res[i]
            if (minv < min && minv != 0f) {
                min = minv
                idx = i
            }
        }
        return idx
    }

    private fun rectRule(pivotRow: Int, pivotColumn: Int) {
        val out: Array<FloatArray> = Array(rows, { i -> FloatArray(cols, { j -> table[i][j] }) })

        (0 until cols).forEach {
            out[pivotRow][it] = table[pivotRow][it] / table[pivotRow][pivotColumn]
        }
        (0 until rows).forEach {
            if (it != pivotRow) {
                out[it][pivotColumn] = 0f
            }
        }


        for (i in 0 until rows) {
            for (j in 0 until cols) {
                if (i != pivotRow && j != pivotColumn) {
                    out[i][j] = (table[pivotRow][pivotColumn] * table[i][j]
                            - table[i][pivotColumn] * table[pivotRow][j]) / table[pivotRow][pivotColumn]
                }
            }
        }
        /*out.first()[out.first().lastIndex] = 0f
        for (i in 0 until rows - 1)
            out.first()[out.first().lastIndex] += table.first()[i]*/

        for (i in 0 until rows) {
            for (j in 0 until cols) {
                table[i][j] = out[i][j]
            }
        }

    }

    // Формування таблиці
    private fun buildNextTable(pivotRow: Int, pivotColumn: Int) {

        val pivotValue = table[pivotRow][pivotColumn]
        val pivotRowVals = FloatArray(cols)
        val pivotColumnVals = FloatArray(cols)
        val rowNew = FloatArray(cols)

        // ділимо всі елемени в рядку на головний
        //для йього копіюємо значення з попереднього рядка таблиці в окреме місце
        System.arraycopy(table[pivotRow], 0, pivotRowVals, 0, cols)

        // копіюємо стовпець
        for (i in 0 until rows) {
            pivotColumnVals[i] = table[i][pivotColumn]
        }

        // ділемо рядок на головний
        for (i in 0 until cols) {
            rowNew[i] = pivotRowVals[i] / pivotValue
        }

        // віднімаємо отримане від всіх інших рядків
        for (i in 0 until rows) {
            if (i != pivotRow) {
                for (j in 0 until cols) {
                    val c = pivotColumnVals[i]
                    table[i][j] = table[i][j] - c * rowNew[j]
                }
            }
        }

        // заміняємо весь головний рядок на новий
        System.arraycopy(rowNew, 0, table[pivotRow], 0, rowNew.size)
    }

    // рахуємо відношення

    // знаходження наступного гол стовпця
    private fun findMainColumn(): Int {
        val values = FloatArray(cols)
        val location: Int

        var pos: Int = 0
        var count = 0
        while (pos < cols - 1) {
            if (table[rows - 1][pos] < 0) {
                //System.out.println("negative value found");
                count++
            }
            pos++
        }

        if (count > 1) {
            for (i in 0 until cols - 1) {
                values[i] = Math.abs(table[rows - 1][i])
            }
            location = findMax(values)
        } else {
            location = count - 1
        }

        return location
    }


    private fun findMainCol(data: FloatArray): Int {
        var minimum: Float
        var c = 1
        var location = 0
        minimum = data[0]

        while (c < data.size) {
            if (data[c] < 0) {
                if (data[c] < minimum) {
                    minimum = data[c]
                    location = c
                }
            }
            c++
        }

        return location
    }

    fun findMax(data: FloatArray): Int {
        var maximum: Float
        var c = 1
        var location = 0
        maximum = data[0]

        while (c < data.size) {
            if (data[c] > maximum) {
                maximum = data[c]
                location = c
            }
            c++
        }
        return location
    }

    // перевірка на оптимальність = невід'ємність
    private fun isOptimal(): Boolean {
        table.last().forEach {
            if (it < 0) return false
        }
        return true
    }
}