package io.github.mikeborodin.ado.lab8

import io.github.mikeborodin.ado.lab1.SimplexLauncher
import java.util.Arrays

class Game(val mat: ArrayList<ArrayList<Float>>) {


    //Optimization
    fun optimizeB(mat: ArrayList<ArrayList<Float>>) {
        val toRemove = findDominateCol(mat)
        println("findDominateCol = $toRemove")
        print(mat)

        val optimized = toRemove == -1
        if (optimized) return

        mat.forEach {
            it.removeAt(toRemove)
        }

        println("call optimize")
        optimizeB(mat)
    }

    fun optimizeA(mat: ArrayList<ArrayList<Float>>) {
        val toRemove = findDominatedRow(mat)
        println("findDominateRow = $toRemove")
        print(mat)
        val optimized = toRemove == -1
        if (optimized) return

        mat.removeAt(toRemove)

        println("call optimize")
        optimizeA(mat)
    }

    //Lower
    fun maxOfMinRows(mat: ArrayList<ArrayList<Float>>): Float {
        val d = mat.map { it.min() ?: it.first() }
        return d.max() ?: d.first()
    }

    fun minOfMaxCols(mat: ArrayList<ArrayList<Float>>): Float {
        var maxs = floatArrayOf()
        for (i in 0 until mat[0].size) {
            val col = mat.map { it[i] }
            val m = col.max() ?: col.first()
            maxs += m
        }
        return maxs.min() ?: maxs.first()
    }

    fun findDominateCol(mat: ArrayList<ArrayList<Float>>): Int {
        for (i in 0 until mat[0].size) {
            //Current col
            val others = (0 until mat[0].size) - i
            cols@ for (o in others) {
                for (r in 0 until mat.size) {
                    if (mat[r][i] < mat[r][o]) {
                        continue@cols
                    }
                }
                return i
            }
        }
        return -1
    }

    fun findDominatedRow(mat: ArrayList<ArrayList<Float>>): Int {
        for (i in 0 until mat.size) {
            //Current col
            val others = (0 until mat.size) - i
            rows@ for (o in others) {
                for (c in 0 until mat[i].size) {
                    if (mat[i][c] < mat[o][c]) {
                        continue@rows
                    }
                }
                return o
            }
        }
        return -1
    }


    fun processMat(mat: ArrayList<ArrayList<Float>>) {
        println("Оптимізуємо для В")
        optimizeB(mat)

        println("Оптимізуємо для А")
        optimizeA(mat)

        val lower = maxOfMinRows(mat)
        println("Нижня ціна гри = $lower")
        val upper = minOfMaxCols(mat)
        println("Верхня ціна гри = $upper")
        val sm = convertToSimplex(mat)
        //val dsm = dualize(sm)

        startSimplex(sm)
        startDSimplex(sm)

    }

    fun startSimplex(mat: ArrayList<ArrayList<Float>>) {
        val bVars = mat.size - 1
        val allVars = (mat[0].size - 1) + (mat.size - 1)
        val matrix = Array(mat.size, { i ->
            FloatArray(mat[i].size, { j ->
                mat[i][j]
            })
        })
        SimplexLauncher.solve(bVars, allVars, matrix, {
            val cost = 1f / it.sum()
            println("Dual Simplex: res ${Arrays.toString(it)}")
            println("Sum = "+it.sum())
            println("Отже ціна гри: ${cost}")
            val A = it.map { it * cost }.toFloatArray()
            println("Стратегія гравця A:${Arrays.toString(A)}")

            var B = floatArrayOf(0f)
            val rev = it.map { it * cost }.toFloatArray().reversed()
            rev.forEach { B += it }

            println("Стратегія гравця B: res ${Arrays.toString(B)}")
            println()
            println()
            println()
            println()
        })

    }

    fun startDSimplex(mat: ArrayList<ArrayList<Float>>) {
        val matrix = Array(mat.size, { i ->
            FloatArray(mat[i].size, { j ->
                mat[i][j]
            })
        })

        //Convert
        val cnv = SimplexLauncher.convertMat(matrix)

        val bVars = mat.size - 1
        val allVars = (mat[0].size - 1) + (mat.size - 1)


        cnv.forEachIndexed { i, row ->
            row[0] = row[0] * -1
        }
        /*
        cnv.last().forEachIndexed { i, e ->
            cnv.last()[i] = cnv.last()[i] * -1
        }*/
        /*matrix.last().forEachIndexed { i, e ->
            matrix.last()[i] = matrix.last()[i] * -1
        }*/

        println()
        println()
        println()
        matrix.let {
            SimplexLauncher.solve(bVars, allVars, it, {
                val cost = 1f / it.sum()
                println(it.sum())
//                println("Dual Simplex: res ${Arrays.toString(it)}")
//                println("Отже ціна гри= res ${cost}")
//
//                val B = it.map { it * cost }.toFloatArray()
//                println("Стратегія гравця А: res ${Arrays.toString(B)}")
            })
        }
    }


    private fun dualize(sm: ArrayList<ArrayList<Float>>): ArrayList<ArrayList<Float>> {
        val res = ArrayList<ArrayList<Float>>()
        for (c in 0 until sm[0].size) {
            res.add(ArrayList(sm.map { it[c] }))
        }
        return res
    }

    private fun convertToSimplex(mat: ArrayList<ArrayList<Float>>): ArrayList<ArrayList<Float>> {
        val res = ArrayList<ArrayList<Float>>()
        mat.forEach { arr ->
            res.add(ArrayList(arr.map { it }).apply { add(1f) })
        }
        res.add(ArrayList(mat[0].map { 1f }).apply { add(0f) })
        return res
    }

    fun print(mat: ArrayList<ArrayList<Float>>) {
        for (i in 0 until mat.size) {
            for (j in 0 until mat[i].size) {
                print(String.format("\t%.1f\t", mat[i][j]))
            }
            println()
        }
        println()
    }

    fun solve() {
        print(mat)
        println("Processing")
        processMat(mat)
    }
}
