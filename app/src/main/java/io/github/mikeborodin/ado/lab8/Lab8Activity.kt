package io.github.mikeborodin.ado.lab8

import SimplexTrue
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TableLayout
import android.widget.TableRow
import io.github.mikeborodin.ado.R.layout
import kotlinx.android.synthetic.main.activity_lab8.solveBtn
import kotlinx.android.synthetic.main.activity_main.cols
import kotlinx.android.synthetic.main.activity_main.okBtn
import kotlinx.android.synthetic.main.activity_main.output
import kotlinx.android.synthetic.main.activity_main.rows
import kotlinx.android.synthetic.main.activity_main.table
import java.io.ByteArrayOutputStream
import java.io.PrintStream


class Lab8Activity : AppCompatActivity() {

    var matrix: ArrayList<ArrayList<Float>>? = null

    val default = arrayListOf(
            arrayListOf(3f, 7f, 8f, 2f),
            arrayListOf(6f, 2f, 3f, 9f),
            arrayListOf(4f, 2f, 3f, 2f)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_lab8)
        title = "Game / Lab8 \uD83D\uDE0E"
        okBtn.setOnClickListener {
            val r = rows.text.toString().toInt()
            val c = cols.text.toString().toInt()
            fillTable(r, c, default, table)
        }

        matrix = default
        solveBtn.setOnClickListener {
            matrix?.let {
                val baos = ByteArrayOutputStream()
                val ps = PrintStream(baos)
                val old = System.out
                System.setOut(ps)

                Game(it).solve()

                System.out.flush()
                System.setOut(old)
                println(baos.toString())
                output.text = baos.toString()
            }
        }


        okBtn.callOnClick()
        solveBtn.callOnClick()
    }

    var isGrpah = false
/*
    private fun buildMatrix(r: Int, c: Int): Array<FloatArray>? {
        var matrix: Array<FloatArray> = arrayOf()
        isGrpah = false

        if (r == 4 && c == 4) { //my costs task is here
            matrix = arrayOf()
            isGrpah = true
        }

        if (r == 3 && c == 3) { //my costs task is here
            matrix = default

        } else
            if (r == 3 && c == 7) { //my costs task is here

                matrix += floatArrayOf(-1f, 0f, -3f, -1f, 2f, 1f, 1f, 7f)
                matrix += floatArrayOf(0f, -1f, 2f, 2f, -1f, 1f, 4f, 2f)
                matrix += floatArrayOf(-50f, -80f, -90f, -10f, -50f, 10f, 20f, 0f)

                */
/*
                matrix += floatArrayOf(-1f, 0f, -1f, -4f, -1f, 1f, 1f, 2f)
                matrix += floatArrayOf(0f, -1f, 2f, -1f, -1f, 1f, 4f, 4f)
                matrix += floatArrayOf(-50f, -80f, -10f, -240f, -105f, 10f, 20f, 0f)*//*

                */
/*
                matrix += floatArrayOf(1f, 0f, 3f, 1f, -2f, -1f, -1f, 7f)
                matrix += floatArrayOf(0f, 1f, -2f, -2f, 1f, -1f, -4f, 2f)
                matrix += floatArrayOf(50f, 80f, 90f, 10f, 50f, -10f, -20f, 0f)
                *//*


            } else
                for (i in 0 until r) {
                    matrix += FloatArray(c, { 0f })
                }
        return matrix
    }
*/

    private fun fillTable(r: Int, c: Int, matrix: ArrayList<ArrayList<Float>>, table: TableLayout) {
        table.removeAllViews()
        for (i in 0 until r) {
            val row = TableRow(this@Lab8Activity)
            row.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)

            for (j in 0 until c) {
                val edit = EditText(this@Lab8Activity).apply {
                    width = 300
                    inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
                    scaleX = .8f
                    scaleY = .8f
                    width = 180
                    width = 180
                    layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT)
                }

                edit.setText(matrix[i][j].toString())

                edit.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        s?.toString()?.takeIf { it.isNotEmpty() }?.let {
                            try {
                                matrix[i][j] = it.toFloat()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }

                })
                row.addView(edit)
            }
            table.addView(row)
        }
    }

    private fun showMatrix(matrix: Array<DoubleArray>) {
        val sb = StringBuilder()
        for (i in 0 until matrix.size) {
            for (j in 0 until matrix[0].size) {
                sb.append("${matrix[i][j]} \t")
            }
            sb.append("\n")
        }
        output.text = sb.toString()

    }


    private fun solve(matrix: Array<FloatArray>?) {
        // коэффициенты из целевой функции
        if (matrix == null) return

        SimplexTrue.basisVars = matrix.size - 1
        SimplexTrue.allVars = SimplexTrue.basisVars * 2

        if (isGrpah) {
            SimplexTrue.basisVars = matrix.size - 1
            SimplexTrue.allVars = SimplexTrue.basisVars * 2
        }

        // коэффициенты из ограничений
        // коэффициенты при базисных переменных
        val basisCoefs = FloatArray(SimplexTrue.basisVars).apply {
            fill(0f)
        }

        // базисные переменные
        val basisVars = IntArray(SimplexTrue.basisVars)
        // базисное решениее переменные

        var nonNegative = true
        matrix.last().forEach {
            nonNegative = it >= 0 && nonNegative
        }
        matrix.forEach {
            nonNegative = it.last() >= 0 && nonNegative
        }

        //if (!nonNegative) {
        println("Тільки невід'ємні значення допускаються. Перевірте вхідні дані, будь  ласка.")
        //    return
        //}

        val bPos = matrix[0].size - 1

        val coefsF = FloatArray(SimplexTrue.allVars).apply {
            //fill(0f)
            (0..SimplexTrue.basisVars).forEach {
                this[it] = matrix.last()[it]
            }
        }


        var coefsLimit = Array(SimplexTrue.basisVars, { FloatArray(SimplexTrue.allVars) })

        (0 until SimplexTrue.basisVars).forEach { r ->
            coefsLimit[r].apply {
                (0 until SimplexTrue.basisVars).forEach { idx ->
                    this[idx] = matrix[r][idx]
                }
                (SimplexTrue.basisVars until SimplexTrue.allVars).forEach { idx ->
                    this[idx] = if (idx - SimplexTrue.basisVars + 1 == r) 1f else 0f
                }
            }
        }


        (0 until SimplexTrue.basisVars).forEach { idx ->
            basisVars[idx] = matrix.last()[idx].toInt()
        }

        val basisSol = FloatArray(SimplexTrue.basisVars).apply {
            (0 until size).forEach { idx ->
                this[idx] = matrix[idx][bPos]
            }
        }
        //Original.gomory();
        SimplexTrue.solve(coefsF, coefsLimit, basisCoefs, basisVars, basisSol)
    }


    fun solveGraph() {
        var visual: Array<FloatArray>? = arrayOf(
                floatArrayOf(1f, 0f, 50f),
                floatArrayOf(0f, 1f, 80f),
                floatArrayOf(1f, -2f, 10f),
                floatArrayOf(-2f, 1f, 10f),
                floatArrayOf(1f, 7f, 0f)
        )


        SimplexTrue.basisVars = 2
        SimplexTrue.allVars = 4

        val basisCoefs = FloatArray(SimplexTrue.basisVars).apply {
            fill(0f)
        }

        val basisVars = IntArray(SimplexTrue.basisVars)
        (0 until SimplexTrue.basisVars).forEach { idx ->
            basisVars[idx] = visual!!.last()[idx].toInt()
        }

        val coefsF = FloatArray(SimplexTrue.allVars).apply {
            this[0] = visual!!.last()[0]
            this[1] = visual.last()[1]
        }

        var coefsLimit = Array(SimplexTrue.basisVars, { FloatArray(SimplexTrue.allVars) })
        (0 until SimplexTrue.basisVars).forEach { r ->
            coefsLimit[r].apply {
                (0 until SimplexTrue.basisVars).forEach { idx ->
                    this[idx] = visual!![r][idx]
                }
                (SimplexTrue.basisVars until SimplexTrue.allVars).forEach { idx ->
                    this[idx] = if (idx - SimplexTrue.basisVars + 1 == r) 1f else 0f
                }
            }
        }

        val bPos = visual!![0].size - 1
        val basisSol = FloatArray(SimplexTrue.basisVars).apply {
            (0 until size).forEach { idx ->
                this[idx] = visual[idx][bPos]
            }
        }
        SimplexTrue.solve(coefsF, coefsLimit, basisCoefs, basisVars, basisSol)

//        val basisVars = IntArray(SimplexTrue.basisVars)
//        basisVars[0]
/*
        // коэффициенты из ограничений
        // коэффициенты при базисных переменных
        // базисные переменные
        // базисное решениее переменные

        val bPos = 43

        val coefsF = FloatArray(SimplexTrue.allVars).apply {
            //fill(0f)
            (0..SimplexTrue.basisVars).forEach {
                this[it] = matrix.last()[it]
            }
        }


        var coefsLimit = Array(SimplexTrue.basisVars, { FloatArray(SimplexTrue.allVars) })

        (0 until SimplexTrue.basisVars).forEach { idx ->
            basisVars[idx] = matrix.last()[idx].toInt()
        }

        val basisSol = FloatArray(SimplexTrue.basisVars).apply {
            (0 until size).forEach { idx ->
                this[idx] = matrix[idx][bPos]
            }
        }
        //Original.gomory();
        */
    }

    fun printOutput() {
        val baos = ByteArrayOutputStream()
        val ps = PrintStream(baos)
        // IMPORTANT: Save the old System.out!
        val old = System.out
        // Tell Java to use your special stream
        System.setOut(ps)
        // Print some output: goes to your special stream
        // Put things back

        System.out.flush()
        System.setOut(old)
        // Show what happened
        println(baos.toString())
        output.text = baos.toString()
    }


}
