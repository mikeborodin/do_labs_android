package io.github.mikeborodin.ado.lab2

import io.github.mikeborodin.ado.lab2.DSimplex.StopCase.NOT_OPTIMAL
import io.github.mikeborodin.ado.lab2.DSimplex.StopCase.OPTIMAL
import io.github.mikeborodin.ado.lab2.DSimplex.StopCase.UNBOUNDED
import java.util.Arrays


class DSimplex(basisCount: Int, allCount: Int) {

    private val rows: Int = basisCount + 1
    private val cols: Int = allCount + 1

    val table: Array<FloatArray>

    private var solutionIsUnbounded = false

    enum class StopCase {
        NOT_OPTIMAL,
        OPTIMAL,
        UNBOUNDED
    }

    init {
        table = Array(rows, { FloatArray(cols) })
    }

    fun print() {
        for (i in 0 until table.size-1) {
            println("Basis $i = X${table[i].indexOf(1f)+1}")
        }

        for (i in 0 until rows) {
            for (j in 0 until cols) {
                print(String.format("\t%.1f\t", table[i][j]))
            }
            println()
        }
        println()
    }

    fun buildTable(data: Array<FloatArray>) {
        for (i in table.indices) {
            System.arraycopy(data[i], 0, table[i], 0, data[i].size)
        }
    }

    // обчислює значення для наступонї тfaблиці
    // використовується у циклі поки не вийде з умови
    fun processStep(): StopCase {
        // крок 1
        if (isOk()) {
            println("Весь b стовпець невід'ємний => Optimal, закінчуєм")
            return OPTIMAL // solution is optimal
        }

        // крок 2
        // знайдемо вхідний стовпець
        val pivotRow = findMainRow()
        println("Головний рядок: $pivotRow")

        // крок 3
        // починаємо пошук головного елемента
        val pivotColumn = findMainCol(pivotRow)

        if (solutionIsUnbounded) {
            return UNBOUNDED
        }

        System.out.println("Головний стовпець: $pivotColumn");
        System.out.println("Головний елемент X[$pivotRow][$pivotColumn]= ${String.format("%.2f",
                table[pivotRow][pivotColumn])}\n\n")

        // крок 4
        // формуємо таблицю
        rectRule(pivotRow, pivotColumn)
        //buildNextTable(pivotRow, pivotColumn)
        // на даному кроці розв ще не оптимальниий
        return NOT_OPTIMAL
    }

    private fun findMainRow(): Int {
        var min = Float.POSITIVE_INFINITY
        var minIdx = 0
        (0 until rows - 1).forEach { index ->
            val t = table[index][0]
            if (table[index][0] < min && table[index][0] < 0) {
                min = table[0][index]
                minIdx = index
            }
        }
        return minIdx
    }


    private fun rectRule(pivotRow: Int, pivotColumn: Int) {
        val out: Array<FloatArray> = Array(rows, { i -> FloatArray(cols, { j -> table[i][j] }) })

        (0 until cols).forEach {
            out[pivotRow][it] = table[pivotRow][it] / table[pivotRow][pivotColumn]
        }
        (0 until rows).forEach {
            if (it != pivotRow) {
                out[it][pivotColumn] = 0f
            }
        }


        for (i in 0 until rows) {
            for (j in 0 until cols) {
                if (i != pivotRow && j != pivotColumn) {
                    out[i][j] = (table[pivotRow][pivotColumn] * table[i][j]
                            - table[i][pivotColumn] * table[pivotRow][j]) / table[pivotRow][pivotColumn]
                }
            }
        }
        /*out.first()[out.first().lastIndex] = 0f
        for (i in 0 until rows - 1)
            out.first()[out.first().lastIndex] += table.first()[i]*/

        for (i in 0 until rows) {
            for (j in 0 until cols) {
                table[i][j] = out[i][j]
            }
        }

    }

    // Формування таблиці
    private fun buildNextTable(pivotRow: Int, pivotColumn: Int) {

        val pivotValue = table[pivotRow][pivotColumn]
        val pivotRowVals = FloatArray(cols)
        val pivotColumnVals = FloatArray(cols)
        val rowNew = FloatArray(cols)

        // ділимо всі елемени в рядку на головний
        //для йього копіюємо значення з попереднього рядка таблиці в окреме місце
        System.arraycopy(table[pivotRow], 0, pivotRowVals, 0, cols)

        // копіюємо стовпець
        for (i in 0 until rows) {
            pivotColumnVals[i] = table[i][pivotColumn]
        }

        // ділемо рядок на головний
        for (i in 0 until cols) {
            rowNew[i] = pivotRowVals[i] / pivotValue
        }

        // віднімаємо отримане від всіх інших рядків
        for (i in 0 until rows) {
            if (i != pivotRow) {
                for (j in 0 until cols) {
                    val c = pivotColumnVals[i]
                    table[i][j] = table[i][j] - c * rowNew[j]
                }
            }
        }

        // заміняємо весь головний рядок на новий
        System.arraycopy(rowNew, 0, table[pivotRow], 0, rowNew.size)
    }

    // рахуємо відношення
    private fun findMainCol(pivotRow: Int): Int {
        var res = FloatArray(cols)

        for (i in 1 until cols) {
            if (table[pivotRow][i] < 0) {
                res[i] = table[pivotRow][i]
            }
        }

//        if (res.min() == 0f) {
//            this.solutionIsUnbounded = true
//        } else {


        for (i in 1 until cols) {
            //val v = res[i]
            val div = res[i]
            val piv = table[table.lastIndex][i]
            if (res[i] != 0f && piv != 0f) {
                res[i] = piv / res[i]
                //res[i] = Math.abs(res[i])
                println("${String.format("%.1f", div)} / ${String.format("%.1f",
                        piv)}   = ${String.format("%.1f", res[i])}\t")
            }
        }
//        }
        println("Відношення (Тета):")
        println(Arrays.toString(res))
        return findMin(res)
    }

    // знаходження наступного гол стовпця
    private fun findMainColumn(): Int {
        val values = FloatArray(cols)
        val location: Int

        var pos: Int = 0
        var count = 0
        while (pos < cols - 1) {
            if (table[rows - 1][pos] < 0) {
                //System.out.println("negative value found");
                count++
            }
            pos++
        }

        if (count > 1) {
            for (i in 0 until cols - 1) {
                values[i] = Math.abs(table[rows - 1][i])
            }
            location = findMax(values)
        } else {
            location = count - 1
        }

        return location
    }


    private fun findMin(data: FloatArray): Int {
        var minimum: Float = Float.POSITIVE_INFINITY
        var pos = 0
        (0 until data.size).forEach {
            if (minimum > data[it] && data[it] != 0f) {
                minimum = data[it]
                pos = it
            }
        }
        return pos
    }

    private fun findMax(data: FloatArray): Int {
        var maximum: Float
        var c = 1
        var location = 0
        maximum = data[0]

        while (c < data.size) {
            if (maximum < data[c]) {
                maximum = data[c]
                location = c
            }
            c++
        }
        return location
    }

    // перевірка на оптимальність = невід'ємність
    fun isOk(): Boolean {
        for (i in 0 until rows - 1) {
            if (table[i][0] < 0) {
                return false
            }
        }
        return true
    }
}