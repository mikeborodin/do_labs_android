package io.github.mikeborodin.ado.start

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.github.mikeborodin.ado.R
import io.github.mikeborodin.ado.lab1.Lab1Activity
import io.github.mikeborodin.ado.lab2.Lab2Activity
import io.github.mikeborodin.ado.lab3.Lab3Activity
import io.github.mikeborodin.ado.lab4.Lab4Activity
import io.github.mikeborodin.ado.lab5.Lab5Activity
import io.github.mikeborodin.ado.lab6.Lab6Activity
import io.github.mikeborodin.ado.lab8.Lab8Activity
import kotlinx.android.synthetic.main.activity_start.lab1Btn
import kotlinx.android.synthetic.main.activity_start.lab2Btn
import kotlinx.android.synthetic.main.activity_start.lab3Btn
import kotlinx.android.synthetic.main.activity_start.lab4Btn
import kotlinx.android.synthetic.main.activity_start.lab5Btn
import kotlinx.android.synthetic.main.activity_start.lab6Btn
import kotlinx.android.synthetic.main.activity_start.lab7Btn
import kotlinx.android.synthetic.main.activity_start.lab8Btn

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        lab1Btn.setOnClickListener {
            startActivity(Intent(this, Lab1Activity::class.java))
        }
        lab2Btn.setOnClickListener {
            startActivity(Intent(this, Lab2Activity::class.java))
        }
        lab3Btn.setOnClickListener {
            startActivity(Intent(this, Lab3Activity::class.java))
        }
        lab4Btn.setOnClickListener {
            startActivity(Intent(this, Lab4Activity::class.java))
        }

        lab5Btn.setOnClickListener {
            startActivity(Intent(this, Lab5Activity::class.java))
        }

        lab6Btn.setOnClickListener {
            startActivity(Intent(this, Lab6Activity::class.java))
        }

        lab7Btn.setOnClickListener {
            startActivity(Intent(this, Lab1Activity::class.java))
        }

        lab8Btn.setOnClickListener {
            startActivity(Intent(this, Lab8Activity::class.java))
        }

        lab8Btn.callOnClick()
    }
}
