package io.github.mikeborodin.ado.lab5

import java.util.TreeSet

class Edge(var v1: String, var v2: String, var dist: Int)


data class Task(val edges: List<Edge>, var start: String, var end: String)
// Клас - вершина графу
class Vertex(val name: String) : Comparable<Vertex> {
    var distance = Int.MAX_VALUE// MAX_VALUE  =>  infinity
    val neighbours = HashMap<Vertex, Int>()
    var prev: Vertex? = null
    fun printPath() {
        if (this == prev) {
            print(name)
        } else if (prev == null) {
            print("$name( ∞ )")
        } else {
            prev!!.printPath()
            print(" -> $name($distance)")
        }
    }

    //for TreeSet
    override fun compareTo(other: Vertex): Int {
        if (distance == other.distance)
            return name.compareTo(other.name)

        return distance.compareTo(other.distance)
    }

    override fun toString() = "($name, $distance)"
}


class Graph(val edges: List<Edge>,
            val directed: Boolean,
            val showAllPaths: Boolean = false) {

    val graph = HashMap<String, Vertex>(edges.size)

    init {
        //Заповнюємо граф
        for (e in edges) {
            if (!graph.containsKey(e.v1)) graph[e.v1] = Vertex(e.v1)
            if (!graph.containsKey(e.v2)) graph[e.v2] = Vertex(e.v2)
        }

        //Заповнюємо сусідів для кожної вершини
        for (e in edges) {
            graph[e.v1]!!.neighbours[graph[e.v2]!!] = e.dist
            if (!directed) graph[e.v2]!!.neighbours[graph[e.v1]!!] = e.dist
        }
    }

    // Друкує шлях до заданої точки
    fun printPath(endName: String) {
        if (!graph.containsKey(endName)) {
            println("Заданої точки немає у графі '$endName'")
            return
        }

        graph[endName]!!.printPath()
        println()
        if (showAllPaths) printAllPaths() else println()
    }

    //Друкує усі шляхи
    private fun printAllPaths() {
        for (v in graph.values) {
            v.printPath()
            println()
        }
        println()
    }


    //Стартуємо з заданої точки :)
    fun initDijkstra(startName: String) {
        if (!graph.containsKey(startName)) {
            println("Стартової точки немає у графі '$startName'")
            return
        }

        val startVertex = graph[startName]
        val t = TreeSet<Vertex>()

        //Виставляємо початкові значення MAX_VALUE = Нескінченність
        for (v in graph.values) {
            v.prev = if (v == startVertex) startVertex else null
            v.distance = if (v == startVertex) 0 else Int.MAX_VALUE
            t.add(v)
        }
        dijkstraMethod(t)
    }

    // алгоритм Дейкстри
    private fun dijkstraMethod(vertexTree: TreeSet<Vertex>) {
        while (!vertexTree.isEmpty()) {
            printAllPaths()

            val u = vertexTree.pollFirst() //Повертає точки найближчі по відстані
            // Якщо точка недосяжна - виходимо

            if (u.distance == Int.MAX_VALUE) break
            //Якщо досяжна - дивимось кожного сусіда
            for (a in u.neighbours) {
                val v = a.key // Сусідня точка
                val alternateDist = u.distance + a.value
                if (alternateDist < v.distance) {
                    // Новий кращий шлях знайдено
                    // Тепер будемо використовувати його
                    vertexTree.remove(v)
                    v.distance = alternateDist
                    v.prev = u
                    vertexTree.add(v)
                }
            }
        }
    }

    fun log(s: String) {
        print(s)
    }


    //FLOYD

    fun floydMethod(weights: Array<IntArray>, vertNumber: Int) {

        //Ініціалізація матриці
        val dist = Array(vertNumber) { i ->
            DoubleArray(vertNumber) { j ->
                if (i != j) {
                    Double.POSITIVE_INFINITY
                } else 0.0
            }
        }
        for (w in weights) dist[w[0] - 1][w[1] - 1] = w[2].toDouble()
        val next = Array(vertNumber) { IntArray(vertNumber) }
        for (i in 0 until next.size) {
            for (j in 0 until next.size) {
                if (i != j) next[i][j] = j + 1
            }
        }
        //Головна логіка алгоритму
        for (k in 0 until vertNumber) {
            log("\n####### Крок №$k #######\n")
            for (i in 0 until vertNumber) {
                for (j in 0 until vertNumber) {
                    if (dist[i][k] + dist[k][j] < dist[i][j]) {
                        log("\ni=$i, j=$j ${dist[i][k] + dist[k][j]} < ${dist[i][j]} => Оновлюємо матрицю\n")

                        dist[i][j] = dist[i][k] + dist[k][j]
                        next[i][j] = next[i][k]
                    }
                }
            }

            //printDistMat
            printDistMat(dist)
        }

        log("\n####### Результати (відстань - шлях) #######\n")
        printFloyd(dist, next)
    }

    fun printFloyd(dist: Array<DoubleArray>, next: Array<IntArray>) {
        var u: Int
        var v: Int
        var path: String
        println("pair dist path")
        for (i in 0 until next.size) {
            for (j in 0 until next.size) {
                if (i != j) {
                    u = i + 1
                    v = j + 1
                    val d = if (dist[i][j] != Double.POSITIVE_INFINITY) "${dist[i][j].toInt()}" else "∞"
                    path = ("%d => %d    %s   :  %s").format(u, v, d, u)
                    do {
                        u = next[u - 1][v - 1]
                        path += " => " + u
                    } while (u != v)
                    println(path)
                }
            }
            println()
        }
    }

    fun printDistMat(dist: Array<DoubleArray>) {
        for (i in 0 until dist.size) {
            for (j in 0 until dist[i].size) {
                if (dist[i][j] == Double.POSITIVE_INFINITY)
                    log("\t∞\t") else log("\t${dist[i][j]}\t")
            }
            println()
        }
    }

}



val directed = false

fun solveNetworkStreamDijkstra(task: Task) {
    with(Graph(task.edges, directed, true)) {
        //Старт!
        log("\n---- Мод. Алгоритм Дейкстри ----\n")
        print(if (directed) "Направлений граф, ${graph.size} вершин\n: " else "Ненаправлений граф, ${graph.size} вершин\n\n")
        initDijkstra(task.start)
        printPath(task.end)
    }
}

fun solveNetworkStreamFloyd(task: Task) {

    with(Graph(task.edges, directed, true)) {
        //Старт!
        log("\n---- Алгоритм Флойда ----\n")

        var weights = task.edges.map {
            intArrayOf(
                    Integer.valueOf(it.v1),
                    Integer.valueOf(it.v2),
                    it.dist)
        }.toTypedArray()

        if (!directed) {
            var toAdd = arrayOf<IntArray>()
            weights.forEach {
                toAdd += intArrayOf(it[1], it[0], it[2])
            }
            weights += toAdd
        }

        val nVertices = weights.map { it.max() }.toTypedArray().maxBy { it!! }

        floydMethod(weights, nVertices!!.toInt())
    }
}

