package io.github.mikeborodin.ado.lab3

import java.lang.Double.compare
import java.util.Arrays
import java.util.LinkedList

class Transport(val input: Array<FloatArray>) {

    var basicPlanCost = -1f
    var optimizedPlanCost = -1f

    var isProblemClosed: Boolean = true
    private var supply: FloatArray
    private var demand: FloatArray

    private val mPlan: Array<Array<ProductShipment>>
    private val costs: Array<FloatArray>

    class ProductShipment(
            var quantity: Float,
            val costPerUnit: Float,
            val r: Int,
            val c: Int)

    init {
        val numOfSuppliers = input.size - 1
        val numOfConsumers = input[0].size - 1
        supply = FloatArray(numOfSuppliers) { i -> input[i].last() }
        demand = FloatArray(numOfConsumers) { j -> input.last()[j] }
        val totalSupply = supply.sum()
        val totalDemand = demand.sum()
        isProblemClosed = totalDemand == totalDemand
        if (isProblemClosed)
            log("∑a = ∑b (Задача закритого типу)\n")
        else log("∑a != ∑b (Задача відкритого типу)\n")

        println()

        if (totalSupply > totalDemand)
            demand += totalSupply - totalDemand
        else if (totalDemand > totalSupply)
            supply += totalDemand - totalSupply

        mPlan = Array(supply.size) { Array(demand.size) { ZERO_PLAN } }
        costs = Array(supply.size) { i -> FloatArray(demand.size, { j -> input[i][j] }) }
    }

    fun northWestCornerRule() {
        var northwest = 0
        for (r in 0 until supply.size) {
            for (c in northwest until demand.size) {
                val lestQuantity = minOf(supply[r], demand[c])
                log("Розглянемо рядок $r  стовпець $c\n Мінімальне з [${supply[r]}, ${demand[c]}] = $lestQuantity\n")

                if (lestQuantity > 0f) {
                    mPlan[r][c] = ProductShipment(lestQuantity, costs[r][c], r, c)
                    supply[r] -= lestQuantity
                    demand[c] -= lestQuantity
                    if (supply[r] == 0f) {
                        northwest = c
                        break
                    }
                }
            }
            print()
        }
    }

    fun potentialOptimizationMethod() {
        var reductionMax = 0f
        var move: Array<ProductShipment>? = null
        var leaving = ZERO_PLAN
        checkDegenerate()

        for (r in 0 until supply.size) {
            for (c in 0 until demand.size) {
                if (mPlan[r][c] != ZERO_PLAN) continue
                //Переходимо по незадіяних клітинках таблиці
                val trial = ProductShipment(0f, costs[r][c], r, c)
                val path = buildClosedPath(trial)
                var reduction = 0f
                var lowestQuantity = Float.MAX_VALUE
                var leavingCandidate = ZERO_PLAN

                //Знак може бути +(true)  або -
                var sign = true
                for (s in path) {
                    if (sign) {
                        reduction += s.costPerUnit
                    } else {
                        reduction -= s.costPerUnit
                        if (s.quantity < lowestQuantity) {
                            leavingCandidate = s
                            lowestQuantity = s.quantity
                        }
                    }
                    sign = !sign
                }

                //Знаходимо що дає найбільшу економію
                if (reduction < reductionMax) {
                    move = path
                    leaving = leavingCandidate
                    reductionMax = reduction
                }

            }
        }
        log("Максимальна економія = $reductionMax\n")
        print()

        if (move != null) {
            val q = leaving.quantity
            var sign = true
            for (s in move) {
                s.quantity += if (sign) q else -q
                mPlan[s.r][s.c] = if (s.quantity == 0f) ZERO_PLAN else s
                sign = !sign
            }
            // Рекурсивне продовження алгоритму, якщо план не оптимальний
            log("Продовжуємо алгоритм\n")
            potentialOptimizationMethod()
        }
    }

    private fun convertMatrixToList() = LinkedList<ProductShipment>(mPlan.flatten().filter { it != ZERO_PLAN })

    private fun buildClosedPath(s: ProductShipment): Array<ProductShipment> {
        val path = convertMatrixToList()
        path.addFirst(s)
        // Видаляєм елементи що не мають вертикального і горизонтального сусідів

        while (path.removeIf {
                    val nbrs = getCellNeighbors(it, path)
                    nbrs[0] == ZERO_PLAN || nbrs[1] == ZERO_PLAN
                });
        // place the remaining elements in the correct plus-minus order
        // розставляємо + -
        val ships = Array(path.size) { ZERO_PLAN }
        var prev = s
        for (i in 0 until ships.size) {
            ships[i] = prev
            prev = getCellNeighbors(prev, path)[i % 2]
        }
        return ships
    }

    private fun getCellNeighbors(s: ProductShipment, lst: LinkedList<ProductShipment>): Array<ProductShipment> {
        val nbrs = Array<ProductShipment>(2) { ZERO_PLAN }
        for (o in lst) {
            if (o != s) {
                if (o.r == s.r && nbrs[0] == ZERO_PLAN)
                    nbrs[0] = o
                else if (o.c == s.c && nbrs[1] == ZERO_PLAN)
                    nbrs[1] = o
                if (nbrs[0] != ZERO_PLAN && nbrs[1] != ZERO_PLAN) break
            }
        }
        return nbrs
    }

    //Перевірка на виродженість
    private fun checkDegenerate() {
        val eps = Float.MIN_VALUE
        if (supply.size + demand.size - 1 != convertMatrixToList().size) {
            log("\nПеревіримо: ${supply.size} + ${demand.size} - 1 != ${convertMatrixToList().size}\n")
            log("\n=> План є виродженим\n")
            for (r in 0 until supply.size) {
                for (c in 0 until demand.size) {
                    if (mPlan[r][c] == ZERO_PLAN) {
                        val dummyShipment = ProductShipment(eps, costs[r][c], r, c)
                        if (buildClosedPath(dummyShipment).isEmpty()) {
                            mPlan[r][c] = dummyShipment
                            return
                        }
                    }
                }
            }
        } else {
            log("\n${supply.size} + ${demand.size} - 1 == ${convertMatrixToList().size}\n")
            log("\nПлан не є виродженим\n")
        }
    }

    //Виводимо на поточний план інтерфейс :)
    fun print() {
        println()
        println()
        var totalCosts = 0.0

        for (r in 0 until supply.size) {
            for (c in 0 until demand.size) {
                val s = mPlan[r][c]
                if (s != ZERO_PLAN && s.r == r && s.c == c) {
                    print("\t\t%.1f\t\t".format(s.quantity))
                    totalCosts += s.quantity * s.costPerUnit
                } else print("\t\t - \t\t")
            }
            print(supply[r])
            println()
        }
        demand.forEach { print(" %.1f ".format(it)) }
        println("\nЦіна плану: ${String.format(" %.1f ", Math.round(totalCosts * 10f) / 10f)}\n")
        println()
        println()

    }

    fun log(s: String) {
        print(s)
    }

    //Vogel
    var deltaSupply = floatArrayOf()
    var deltaDemand = floatArrayOf()
    val rowDone = BooleanArray(supply.size)
    val colDone = BooleanArray(demand.size)
    val results = Array(supply.size) { FloatArray(demand.size) }

    fun vogelMethod() {
        var supplySum = supply.sum()
        var totalCost = 0f
        var step = 0
        while (supplySum > 0) {
            log("\nКрок № ${++step}\n")

            val cell = nextCell()
            val r = cell[0]
            val c = cell[1]

            val q = minOf(demand[c], supply[r])
            log("\nМінімум  [${demand[c]}, ${supply[r]}] = $q\n")
            log("\nПостачальник $c ${demand[c]} - $q = ")
            demand[c] -= q
            log("${demand[c]};\n")

            if (demand[c] == 0f) colDone[c] = true
            supply[r] -= q
            if (supply[r] == 0f) rowDone[r] = true
            results[r][c] = q
            supplySum -= q
            totalCost += q * costs[r][c]
        }
        //Виводимо для використання методом потенціалыв

        printv()
    }

    fun nextCell(): IntArray {
        //Макс виграш по рядках
        val res1 = maxPenalty(supply.size, demand.size, true)

        //Пщ стовпцях
        val res2 = maxPenalty(demand.size, supply.size, false)

        if (res1[3] == res2[3]) return if (res1[2] < res2[2]) res1
        else res2
        return if (res1[3] > res2[3]) res2
        else res1
    }

    fun maxPenalty(len1: Int, len2: Int, isRow: Boolean): IntArray {
        var maxDelta = Int.MIN_VALUE
        var pc = -1
        var pm = -1
        var mc = -1
        for (i in 0 until len1) {
            val cellIsDone = if (isRow) rowDone[i] else colDone[i]
            if (cellIsDone) continue

            val res = minimalDiff(i, len2, isRow)
            if (res[0] > maxDelta) {
                maxDelta = res[0]  // Записуэмо поточну максимальну різницю
                pm = i       // Позиція максимальної різниці
                mc = res[1]  // Найменьша ціна
                pc = res[2]  // Позиція НЦ
            }
        }
        return if (isRow) intArrayOf(pm, pc, mc, maxDelta) else
            intArrayOf(pc, pm, mc, maxDelta)
    }

    fun minimalDiff(j: Int, len: Int, isRow: Boolean): IntArray {
        var min1 = Float.MAX_VALUE
        var min2 = min1
        var minPos = -1

        for (i in 0 until len) {
            val done = if (isRow) colDone[i] else rowDone[i]
            if (done) continue
            val c = if (isRow) costs[j][i] else costs[i][j]
            if (c < min1) {
                min2 = min1
                min1 = c
                minPos = i
            } else if (c < min2) min2 = c
        }
        if (isRow)
            log("\nМінімуми по рядку № $j = [$min1, $min2]\n")
        else
            log("\nМінімуми по стовпцю № $j = [$min1, $min2]\n")

        return intArrayOf((min2 - min1).toInt(), min1.toInt(), minPos)
    }

    fun printv() {
        println()
        println()
        var totalCosts = 0.0
        for (r in 0 until supply.size) {
            for (c in 0 until demand.size) {
                print("\t%.1f\t".format(results[r][c]))
                totalCosts += results[r][c] * costs[r][c]
            }
            print(supply[r])
            println()
        }
        demand.forEach { print(" %.1f ".format(it)) }
        println("\nЦіна плану: $totalCosts\n")
    }

    //Min method start

    var cellsDone: Array<BooleanArray> = Array(input.size, { BooleanArray(input[0].size, { false }) })

    fun minMethod() {
        //min price in table

        while (demand.sum() != 0f || supply.sum() != 0f) {

            var minPrice = Float.MAX_VALUE
            var minR = -1
            var minC = -1

            for (i in 0 until supply.size) {
                for (j in 0 until demand.size) {
                    //
                    if (costs[i][j] < minPrice && !rowDone[i] && !colDone[j]) {
                        cellsDone[i][j] = true

                        minPrice = costs[i][j]
                        minR = i
                        minC = j
                    }
                }
            }

            val minQuantity = minOf(supply[minR], demand[minC])
            log("\nНайменша ціна $minPrice$ на перетині ${minR}, ${minC}\n")
            log("\n=> MIN( ${supply[minR]}, ${demand[minC]}) = ${minQuantity}\n")

            supply[minR] -= minQuantity
            demand[minC] -= minQuantity

            mPlan[minR][minC] = ProductShipment(minQuantity, minPrice, minR, minC)

            if (supply[minR] == 0f) {
                rowDone[minR] = true
                log("\nВиробник $minR порожній!\n")
            } else if (demand[minC] == 0f) {
                colDone[minC] = true
                log("\nКористувач $minC повний!\n")
            }
            print()
        }
        print()
    }

    companion object {
        // Поставка нічого
        private val ZERO_PLAN = ProductShipment(0f, 0f, -1, -1)
    }

    fun getCost(): Float {
        var totalCosts = 0f
        for (r in 0 until supply.size) {
            for (c in 0 until demand.size) {
                totalCosts += mPlan[r][c].quantity * costs[r][c]
            }
            print(supply[r])
        }
        return totalCosts
    }


    fun printD(demand: FloatArray, supply: FloatArray) {
        println()
        println()
        var totalCosts = 0.0

        val sb = StringBuilder()

        for (r in 0 until supply.size) {
            for (c in 0 until demand.size) {
                val s = mPlan[r][c]
                if (s != ZERO_PLAN && s.r == r && s.c == c) {
                    print("\t\t%.1f\t\t".format(s.quantity))
                    totalCosts += s.quantity * costs[r][c]
                    sb.append("+ ${s.quantity} * ${costs[r][c]}")
                } else print("\t\t - \t\t")
            }
            print(supply[r])
            println()
        }

        demand.forEach { print(" %.1f ".format(it)) }
        println()
        println("\nТарифи\n")
        for (r in 0 until supply.size) {
            for (c in 0 until demand.size) {
                val s = mPlan[r][c]
                if (s != ZERO_PLAN && s.r == r && s.c == c) {
                    print("\t\t(%.1f)\t\t".format(costs[r][c]))
                } else {
                    print("\t\t%.1f\t\t".format(costs[r][c]))
                }
            }
            print(supply[r])
            println()
            println()
        }
        println()
        println("\nЦіна плану: ${sb.toString()} ${String.format(" %.1f ", Math.round(totalCosts * 10f) / 10f)}\n")

    }

    fun diffRentMethod() {
        //1 step
        //find cells toFill
        var step = 0
        val tariffs = Array(supply.size, { i -> FloatArray(demand.size, { j -> costs[i][j] }) })

        var optimal = false
        while (!optimal) {
            step++
            log("\nPlan is not optimal yet!\n")
            val supply = supply.clone()
            val demand = demand.clone()

            var toFill = arrayOf<ProductShipment>()
            for (c in 0 until demand.size) {
                var minValue = Float.MAX_VALUE
                for (r in 0 until supply.size) {
                    if (minValue > tariffs[r][c]) {
                        minValue = tariffs[r][c]
                    }
                }
                var minsInCol = intArrayOf()
                for (r in 0 until supply.size) {
                    if (tariffs[r][c] == minValue) {
                        minsInCol += r
                        toFill += ProductShipment(0f, tariffs[r][c], r, c)
                    }
                }
                log("\nMin in col =${c} ${minValue} (at ${Arrays.toString(minsInCol)})\n")
            }


            //val sortedPoints = arrayOf<ProductShipment>()

            var sortedPoints = arrayOf<ProductShipment>()

            val usedPoints = hashMapOf<Int, ProductShipment>()

            //make better rule
            //
            for (r in 0 until supply.size) {
                val pointsInRow = toFill.filter {
                    it.r == r && !usedPoints.containsKey(it.hashCode())
                }

                if (pointsInRow.size == 1) {
                    sortedPoints += pointsInRow[0]
                    usedPoints[pointsInRow[0].hashCode()] = pointsInRow[0]
                }
            }

            for (c in 0 until demand.size) {
                val pointsInCol = toFill.filter {
                    it.c == c && !usedPoints.containsKey(it.hashCode())
                }
                if (pointsInCol.size == 1) {
                    sortedPoints += pointsInCol[0]
                    usedPoints[pointsInCol[0].hashCode()] = pointsInCol[0]
                }
            }

            for (r in 0 until supply.size) {
                val pointsInRow = toFill.filter {
                    it.r == r && !usedPoints.containsKey(it.hashCode())
                }

                if (pointsInRow.size == 1) {
                    sortedPoints += pointsInRow[0]
                    usedPoints[pointsInRow[0].hashCode()] = pointsInRow[0]
                }
            }

            //Fill
            for (p in sortedPoints) {
                val c = p.c
                val r = p.r

                val q = minOf(demand[c], supply[r])

                demand[c] -= q
                supply[r] -= q
                mPlan[r][c] = ProductShipment(q, tariffs[r][c], r, c)

            }

            val rowBalances = mPlan.mapIndexed { r, _ -> getRowBalance(r, toFill.filter { it.r == r }, demand, supply) }
                    .toFloatArray()
            fixZeroBalanceCase(rowBalances, toFill)

            val rowBalancesBool = rowBalances.map { compare(it.toDouble(), 0.0) >= 0 }


            for (r in 0 until mPlan.size) {
                log("\nRow #${r} balance: ${if (rowBalances[r] > 0) "+" else ""}${rowBalances[r]}\n")
            }

            //Diffs and rent
            var diffs = floatArrayOf()


            printD(demand, supply)


            for (c in 0 until demand.size) {

                var negative = floatArrayOf()
                var positive = floatArrayOf()
                for (r in 0 until supply.size) {
                    if (rowBalancesBool[r]) {
                        //до нього тарифом, записаним у надлишковому рядку
//                        log("\nNegtive costs[$r][$c] = ${costs[r][c]} $c\n")
                        positive += tariffs[r][c]
                        //Для кожного стовпчика знаходимо різницю між обведеним тарифом у від’ємному рядку
                    } else if (!rowBalancesBool[r] && toFill.any { it.r == r && it.c == c }) {
//                        log("\nPositive costs[$r][$c] = ${costs[r][c]} $c\n")
                        negative += tariffs[r][c]
                    }
                }

                negative.forEach { n ->
                    positive.forEach { p ->
                        diffs += p - n
                    }
                }
                println()
            }

            val renta = diffs.filter { it != 0f }.min()
            if (renta == null) {
                log("\nПромiжна рента $renta == 0 -> Завершуємо :)\n")
            } else {
                log("\nПромiжна рента = $renta;\n")
                for (r in 0 until supply.size)
                    if (!rowBalancesBool[r]) {
                        log("\nДодаєм проміжну ренту до рядка №$r\n")
                        for (j in 0 until demand.size)
                            tariffs[r][j] += renta
                    }
            }
            println()
            if (supply.sum() == 0f && demand.sum() == 0f) {
                optimal = true
            }
        }
    }


    private fun fixZeroBalanceCase(rowBalances: FloatArray, toFill: Array<ProductShipment>) {
        if (rowBalances.min() == 0f && rowBalances.max() == 0f) return

        for (r in 0 until rowBalances.size) {
            if (rowBalances[r] == 0f) {
                if (toFill.any { it.r == r }) {
                    rowBalances[r] = -0f
                    if(true)return
                    toFill.filter { it.r == r }.forEach { rp ->
                        if (toFill.any { it.c == rp.c && rowBalances[it.r] < 0 }) {
                            rowBalances[r] = -0f
                        }
                    }
                }
            }
        }
    }


    fun getRowBalance(r: Int, pointsOfRow: List<ProductShipment>, demand: FloatArray, supply: FloatArray): Float {
        val minus = pointsOfRow.map { demand[it.c] }.sum()
        return supply[r] - minus
    }
}

fun solveTransportNW(input: Array<FloatArray>) {
    with(Transport(input.clone())) {
        //Старт!
        log("1 етап. Пошук опорного плану\n")
        log("---- Метод Пів-Зах кута ----\n")
        northWestCornerRule()
        print()
        var init = getCost()
        log("---- Цiна плану за м. Пів-Зах кута = ${init}----\n")
        log("2 етап. Оптимізація плану\n")
        log("---- Метод Потенціалів ----\n")
        potentialOptimizationMethod()
        log("---- Цiна плану після м. Потенціалів = ${getCost()}\nОптимізація = ${init - getCost()}----\n")

    }
}

fun solveTransportVogel(input: Array<FloatArray>) {
    // 2)
    with(Transport(input.clone())) {
        log("1 етап. Пошук опорного плану\n")
        log("---- Метод Фойгеля ----\n\n")
        vogelMethod()
        var init = getCost()
        log("---- Цiна плану за м. Фойгеля = ${init}----\n")
        log("2 етап. Оптимізація плану\n")
        log("---- Метод Потенціалів ----\n\n")
        potentialOptimizationMethod()
        log("---- Цiна плану після м. Потенціалів = ${getCost()}\nОптимізація = ${init - getCost()}----\n")

    }
}

fun solveTransportMin(input: Array<FloatArray>) {
    // 2)
    with(Transport(input.clone())) {
        log("\n1 етап. Пошук опорного плану\n")
        log("\n---- Метод Мінінімального ел. ----\n\n")
        minMethod()
        var init = getCost()
        log("\n---- Цiна плану за м. Мінінімального ел = ${init}----\n")
        log("2 етап. Оптимізація плану\n")
        log("\n---- Метод Потенціалів ----\n\n")
        potentialOptimizationMethod()
        log("\n---- Цiна плану після м. Потенціалів = ${getCost()}" +
                "\nОптимізація = ${init - getCost()}$----\n")
    }
}

fun solveTransportDiff(input: Array<FloatArray>) {
    with(Transport(input.clone())) {
        log("\n---- Метод Диф. рент. ----\n\n")
        diffRentMethod()
    }
}
