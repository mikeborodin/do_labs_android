package io.github.mikeborodin.ado.lab6

import io.github.mikeborodin.ado.lab5.Edge
import io.github.mikeborodin.ado.lab5.Task
import io.github.mikeborodin.ado.lab5.Vertex
import java.util.Arrays

data class Cut(var v: Array<Vertex>, var maxFlow: Int, var parent: Int)

class MaxFlow(val task: Task, val numv: Int) {

    var directed = true
    val graph = HashMap<String, Vertex>(task.edges.size)

    init {
        //Заповнюємо граф
        for (e in task.edges) {
            if (!graph.containsKey(e.v1)) graph[e.v1] = Vertex(e.v1)
            if (!graph.containsKey(e.v2)) graph[e.v2] = Vertex(e.v2)
        }

        //Заповнюємо сусідів для кожної вершини
        for (e in task.edges) {
            graph[e.v1]!!.neighbours[graph[e.v2]!!] = e.dist
            if (!directed) graph[e.v2]!!.neighbours[graph[e.v1]!!] = e.dist
        }


    }

    fun d(v1: String, v2: String): Int {
        val e = task.edges.firstOrNull { it.v1 == v1 && it.v2 == v2 }
        return e?.dist ?: Int.MAX_VALUE
    }

    fun isConnected(graph: ArrayList<Vertex>) {
        var connected = true;
    }

    fun gomory() {
        val used = hashMapOf<String, Vertex>()
        val cluster = ArrayList(graph.map { it.value })
        val sb = StringBuilder()
        val mat = Array(numv, { IntArray(numv) })

        //We test our magic before starting!
        graph.values.forEach {
            if (it.name.length != 1) throw Exception("Only a b c etc... One char please :)")
            if (mat.size < it.getId()) {
                throw Exception("Wrong Characters!")
            }
        }

        for (step in 0 until numv - 1) {

            //find T and T2
            val t = cluster
                    .find {
                        it.name == task.edges.sortedBy { it.dist }
                                .filter { cluster.contains(graph[it.v1]) }
                                .reversed().first().v1

                                || it.name == task.edges
                                .sortedBy { it.dist }
                                .filter { cluster.contains(graph[it.v2]) }
                                .reversed().first().v2
                    }!!


            val t2 = cluster
                    .find { !used.containsKey(it.name) && it.neighbours.containsKey(t) }

            if (t2 != null) {
                println("cut ${t}, ${t2}")

                cluster.remove(t)
                cluster.remove(t2)

                var eFromCluster = listOf<Edge>()
                cluster.forEach { cn ->
                    task.edges.filter {
                        ((cn.name == it.v1 && it.v2 == t.name) || (cn.name == it.v2 && it.v1 == t.name)) ||
                                ((cn.name == it.v1 && it.v2 == t2.name) || (cn.name == it.v2 && it.v1 == t2.name))
                    }.forEach {
                        eFromCluster += it
                    }
                }

                val tsSum = eFromCluster.sumBy { it.dist }

                val tIdx = t.getId()

            } else {

            }

            printMat(-1, mat)
        }
    }

    fun printMat(step: Int, mat: Array<IntArray>) {
        println("\t\t Крок #$step \t\t")
        println()

        print("\t\t-\t\t")
        graph.values.sortedBy { it.name }.map { it.name }.forEach {
            print("\t ${it} \t")
        }
        println()

        for (i in 0 until mat.size) {
            print("\t${graph.values.sortedBy { it.name }.map { it.name }[i]}\t")
            for (j in 0 until mat[i].size) {
                when {
                    i == j -> print("\t\t - \t\t")
                    mat[i][j] == Int.MAX_VALUE -> print("\t ∞ \t")
                    else -> print("\t\t${mat[i][j]}\t\t")
                }
            }
            println()
        }
        println()
        println()
    }

    fun fillMat(mat: Array<IntArray>) {

        graph.values.sortedBy { it.name }.forEachIndexed { i, eli ->
            graph.values.sortedBy { it.name }.forEachIndexed { j, elj ->
                //Symmetric matrix
                when {
                    eli.neighbours[elj] != null -> {
                        mat[i][j] = eli.neighbours[elj]!!
                        mat[j][i] = eli.neighbours[elj]!!
                    }
                    elj.neighbours[eli] != null -> {
                        mat[i][j] = elj.neighbours[eli]!!
                        mat[j][i] = elj.neighbours[eli]!!
                    }
                    else -> mat[i][j] = Int.MAX_VALUE
                }

            }
        }
    }

    fun processTable(matS: Array<IntArray>) {
        var sums = IntArray(numv)
        val sumsOptimized = IntArray(numv)
        val res = Array(numv, { IntArray(numv) })


        val rowOrder = matS.mapIndexed { i, row -> Pair(i, row) }
                .sortedBy { it.second.filter { it != Int.MAX_VALUE }.sum() }
                .map { it.first }.toTypedArray()

        for (i in rowOrder) {
            val rowSum = matS[i].filter { it != Int.MAX_VALUE }.sum()
            sums[i] = rowSum
            println("Дивимось рядок ${posToLetter(i)}, ∑=$rowSum ${Arrays.toString(matS[i].filter { it != Int.MAX_VALUE }.toIntArray())}")

            for (j in 0 until matS.size) {
                val rowTmp = IntArray(numv, { rj -> matS[i][rj] })
                val colSum = matS.mapIndexed { ind, _ -> matS[ind][j] }
                        .filterIndexed { ind, el -> ind != i && el != Int.MAX_VALUE }.sum()

                if (colSum < rowTmp[j] && rowTmp[j] != Int.MAX_VALUE) {
                    val oldRowTmpJ = rowTmp[j]
                    rowTmp[j] = colSum
                    val rowTmpSum = rowTmp.filter { it != Int.MAX_VALUE }.sum()
                    sumsOptimized[i] = rowTmpSum

                    res[i][j] = sumsOptimized[i]
                    res[j][i] = sumsOptimized[i]

                    println("Мін. розріз : $colSum < ${oldRowTmpJ}, col=${posToLetter(j)}\n => ∑=$rowTmpSum")
                }
            }
            printMat(i, matS)
        }

//        println("Results:")
//        printMat(0, res)

        sums.forEach { print("\t\t${it}\t\t") }
        println()
        sumsOptimized.forEach { print("\t\t${if (it == 0) "-" else it.toString()}\t\t") }
        println()
    }

    private fun posToLetter(j: Int): String {
        return ('a'.toInt() + j).toChar().toString()
    }

    fun gomoryImpl() {
        val mat = Array(numv, { IntArray(numv, { Int.MAX_VALUE }) })
        fillMat(mat)
        println("Input matrix")
        //printMat(0, mat)
        processTable(mat)
    }
}

fun solveNetworkFlowGomory(task: Task, numv: Int) {
    println("\n---- Алгоритм Gomory - Hu ----\n")
    with(MaxFlow(task, numv)) {
        gomoryImpl()
    }
}

fun Vertex.getId(): Int {
    return this.name.toLowerCase().toCharArray().first().toInt() - 'a'.toInt()
}